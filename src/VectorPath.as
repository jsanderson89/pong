package  
{
	/**
	 * ...
	 * @author j
	 */
	public class VectorPath 
	{
		private var _x:Number;
		private var _y:Number;
		
		public function VectorPath(x:Number=0, y:Number=0) 
		{
			_x = x;
			_y = y;
		}
		
		public function get x():Number
		{
			return _x;
		}
		
		public function set x(i:Number):void
		{
			_x = i;
		}
		
		public function get y():Number
		{
			return _y;
		}
		
		public function set y(i:Number):void
		{
			_y = i;
		}
		
		
	}

}