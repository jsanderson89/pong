package  
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.text.Font;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import Keycodes;

	/**
	 * ...
	 * @author j
	 */
	
	public class Game extends Sprite
	{
		private var stageHeight:int;
		private var stageWidth:int;

		private var paddleMargin:int = 30;
		private var ballSize:int = 15;
		private var ballTargetOne:Point;
		private var ballTargetTwo:Point;
		
		private var paddleOneIsServing:Boolean;
		private var paddleTwoIsServing:Boolean;
		private var paddleOnePos:int;
		private var paddleTwoPos:int;
		private var pOneScore:int;
		private var pTwoScore:int;
		
		private var ball:Ball;
		private var pOne:Paddle;
		private var pTwo:Paddle;
		private var net:CenterNet;
		
		//[Embed(source="Assets/MISTRAL.TTF", fontName="font", mimeType="application/x-font-truetype")]
		[Embed(source="Assets/Square.ttf", fontName="font", mimeType="application/x-font-truetype")]
		//[Embed(source="Assets/PetMe.ttf", fontName="font", mimeType="application/x-font-truetype")]
		private var fontClass:Class;
		private var pOneScoreBox:TextField;
		private var pTwoScoreBox:TextField;
		private var tFormat:TextFormat;
		
		public function Game(w:int,h:int) 
		{
			stageHeight = h;
			stageWidth = w;
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		public function init(e:Event=null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init)
			paddleOnePos = paddleMargin;
			paddleTwoPos = (800 - paddleMargin);
			pOneScore = 0;
			pTwoScore = 0;
			
			if (this.stage)
			{
				tFormat = new TextFormat();
				tFormat.font = "font";
				tFormat.color = 0xFFFFFF;
				tFormat.size = 100;
				
				pOneScoreBox = new TextField();
				pOneScoreBox.defaultTextFormat = tFormat;
				pOneScoreBox.selectable = false;
				pOneScoreBox.embedFonts = true;
				pOneScoreBox.x = 0 + pOneScoreBox.width;
				pOneScoreBox.text = "0";
				addChild(pOneScoreBox);
				
				pTwoScoreBox = new TextField();
				pTwoScoreBox.defaultTextFormat = tFormat;
				pTwoScoreBox.selectable = false;
				pTwoScoreBox.embedFonts = true;
				pTwoScoreBox.x = stageWidth - (pTwoScoreBox.width + pTwoScoreBox.width/2);
				pTwoScoreBox.text = "0";
				addChild(pTwoScoreBox);
				
				ball = new Ball(ballSize);

				pOne = new Paddle(Keycodes.W_KEY, Keycodes.S_KEY, stage, stageHeight, ballSize );
				addChild(pOne);
				pOne.centerX = paddleOnePos;
				pOne.centerY = stageHeight / 2;
				ballTargetOne = new Point;
				
				pTwo = new Paddle(Keycodes.UP_KEY, Keycodes.DOWN_KEY, stage, stageHeight,ballSize);
				addChild(pTwo);
				pTwo.centerX = paddleTwoPos;
				pTwo.centerY = stageHeight / 2;
				ballTargetTwo = new Point;
				
				net = new CenterNet();
				addChild(net);
				net.centerX = stageWidth / 2;
				
				setupServeMode(pOne);
			}
		}
		
		private function update(e:Event):void
		{
			if (ball.IsMoving == true)
			{
				checkForEndGame();
			}
			
			if (paddleOneIsServing)
			{
				ballTargetOne.y = pOne.centerY;
				ballTargetOne.x = pOne.centerX + ball.width + 10;
				ball.centerY = ballTargetOne.y;
				ball.centerX = ballTargetOne.x;
			}
			
			else if (paddleTwoIsServing)
			{
				ballTargetTwo.y = pTwo.centerY;
				ballTargetTwo.x = pTwo.centerX - ball.width - 10;
				ball.centerX = ballTargetTwo.x;
				ball.centerY = ballTargetTwo.y;
			}
			
			ball.update();
			
			var hitPoint:int;
			if (ball.hitTestObject(pOne) && ball.BallSpeed.x < 0)
			{
				hitPoint = pOne.calculateDistanceFromCenter(ball.centerX, ball.centerY);
				ball.BallSpeed.x *= -1;
				changeBallAngle(hitPoint);
			}
			
			else if (ball.hitTestObject(pTwo)&& ball.BallSpeed.x > 0)
			{
				hitPoint = pTwo.calculateDistanceFromCenter(ball.centerX, ball.centerY);
				ball.BallSpeed.x *= -1;
				changeBallAngle(hitPoint);
			}

			if (ball.y < 0)
			{
				ball.BallSpeed.y *= -1;
			}
			if ((ball.y + ball.height) > stageHeight)
			{
				ball.BallSpeed.y *= -1;
			}
		}
		
		private function setupServeMode(player:Paddle):void
		{
			addChild(ball);
			addEventListener(Event.ENTER_FRAME, update);
			stage.addEventListener(KeyboardEvent.KEY_DOWN, serve);
			
			if (player == pOne)
			{
				paddleOneIsServing = true;
				ball.BallSpeed = new VectorPath(10, 0);
			}
			
			else if (player == pTwo)
			{
				paddleTwoIsServing = true;
				ball.BallSpeed = new VectorPath(-5, 0);
			}
		}
		
		private function serve(e:KeyboardEvent):void
		{
			if (e.keyCode)
			{
				ball.IsMoving = true;
				deactivateServeModes();
			}
		}
	
		private function deactivateServeModes():void
		{
			paddleOneIsServing = false;
			paddleTwoIsServing = false;
		}
		
		private function checkForEndGame():void
		{
			if (ball.x < 0)
			{
				endGame(pTwo);
			}
				
			if (ball.x > stageWidth)
			{
				endGame(pOne);
			}
		}
		
		private function endGame(winner:Paddle):void
		{
			ball.IsMoving = false;
			removeEventListener(Event.ENTER_FRAME, update)
			removeChild(ball);
				
			if (winner == pOne)
			{
				//trace("pOne wins");
				setupServeMode(pOne);
				pOneScore++;
				pOneScoreBox.text = pOneScore.toString();
			}
			
			else if (winner == pTwo)
			{
				//trace("pTwo wins");
				setupServeMode(pTwo);
				pTwoScore++;
				pTwoScoreBox.text = pTwoScore.toString();
			}
			
			if (pOneScore > 10 || pTwoScore > 10)
			{
				pOneScore = 0;
				pOneScoreBox.text = pOneScore.toString();
				pTwoScore = 0;
				pTwoScoreBox.text = pTwoScore.toString();
			}
		}

		private function changeBallAngle(hitPoint:int):void
		{
			ball.BallSpeed = new VectorPath (ball.BallSpeed.x, hitPoint/-2);
		}
		
	}
}