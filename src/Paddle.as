package  
{
	import flash.display.DisplayObject;
	import flash.display.InteractiveObject;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.geom.Rectangle;
	/**
	 * ...
	 * @author j
	 */
	public class Paddle extends Actor
	{
		private var _color:uint = 0xffffff;

		private var _paddleWidth:int = 20;
		private var _paddleLength:int = 75
		private var _stage:Stage;
		private var _upKey:int;
		private var _downKey:int;
		private var _speed:int = 10;
		private var _upKeyIsPressed:Boolean;
		private var _downKeyIsPressed:Boolean;
		private var _verticalBounds:int;
		
		private var _topFlag:Boolean;
		private var _bottomFlag:Boolean;
		
		public function Paddle(upKey:int, downKey:int, stage:Stage, bounds:int, ballsize:int) 
		{
			_upKey = upKey;
			_downKey = downKey;
			_stage = stage;
			_verticalBounds = bounds;
			
			create();
		}
		
		private function draw():void
		{
			this.graphics.beginFill(_color);
			this.graphics.drawRect(0, 0, _paddleWidth, _paddleLength);
			this.graphics.endFill();
		}
		
		public function create():void
		{
			if (_stage)
			{
				_stage.addEventListener(Event.ENTER_FRAME, controlBounds);
				_stage.addEventListener(Event.ENTER_FRAME, keyHandler);
				_stage.addEventListener(KeyboardEvent.KEY_DOWN, keyPressedHandler);
				_stage.addEventListener(KeyboardEvent.KEY_UP, keyReleasedHandler);
				draw();
			}
		}
		
		private function controlBounds(e:Event):void
		{
			if (y < 0)
			{
				this.y = 0;
				_topFlag = true;
			}
			
			if ((y + _paddleLength) > _verticalBounds)
			{
				var setTo:int = _verticalBounds - _paddleLength
				this.y = setTo;
				_bottomFlag = true;
			}
		}
		
		private function keyHandler(e:Event):void
		{
			if (_upKeyIsPressed && !_topFlag)
			{
				moveUp();
			}
			
			if (_downKeyIsPressed && !_bottomFlag)
			{
				moveDown();
			}
		}
		
		private function moveUp():void
		{
			this.y -= _speed;
			_bottomFlag = false;
		}
		
		private function moveDown():void
		{
			this.y += _speed;
			_topFlag = false;
		}
		
		private function keyPressedHandler(e:KeyboardEvent):void
		{
			if (e.keyCode == _upKey)
			{
				_upKeyIsPressed = true;
			}
			
			else if (e.keyCode == _downKey)
			{
				_downKeyIsPressed = true;
			}
		}
		
		public function calculateDistanceFromCenter(xLocation:Number,yLocation:Number):int
		{
			var collisionPoint:int = this.centerY - yLocation;
			return collisionPoint;
		}
		
		private function keyReleasedHandler(e:KeyboardEvent):void
		{
			if (e.keyCode == _upKey)
			{
				_upKeyIsPressed = false;
			}
			
			else if (e.keyCode == _downKey)
			{
				_downKeyIsPressed = false;
			}
		}
		
		public function set stage(i:Stage):void
		{
			_stage = i;
		}
		
	}

}