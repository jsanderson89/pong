package  
{
	/**
	 * ...
	 * @author j
	 */
	public class CenterNet extends Actor 
	{
		private var _color:uint = 0xffffff;
		
		public function CenterNet():void
		{
			draw();
		}
		
		private function draw():void
		{
			var yloc:int = 8;
			for (var i:int = 0; i <= 30; i++)
			{
				this.graphics.beginFill(_color);
				this.graphics.drawRect(0, yloc, 15, 15);
				this.graphics.endFill();
				yloc += 30;
			}

		}

		
	}

}