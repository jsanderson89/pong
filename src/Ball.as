package  
{
	import flash.display.Stage;
	import flash.events.Event;
	import VectorPath;
	/**
	 * ...
	 * @author j
	 */
	public class Ball extends Actor 
	{
		private var _color:uint = 0xffffff;
		private var _ballsize:int;
		private var _isMoving:Boolean;
		
		private var _ballPath:VectorPath;
		
		public function Ball(size:int) 
		{
			_ballsize = size;
			create();
		}
		
		private function draw():void
		{
			this.graphics.beginFill(_color);
			this.graphics.drawRect(0, 0, _ballsize, _ballsize);
			this.graphics.endFill();
		}
		
		public function create():void
		{
			draw();
			_ballPath = new VectorPath( 0, 0);
		}
		
		public function update():void
		{
			move();
		}
		
		private function move():void
		{
			if (_isMoving)
			{
				this.x += _ballPath.x;
				this.y += _ballPath.y;
			}
		}
		
		public function set IsMoving(i:Boolean):void
		{
			_isMoving = i;
		}
		
		public function get IsMoving():Boolean
		{
			return _isMoving;
		}
		
		public function get BallSpeed():VectorPath
		{
			return _ballPath;
		}
		
		public function set BallSpeed(i:VectorPath):void
		{
			_ballPath = i;
		}
		
	}

}