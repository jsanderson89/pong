Pong


A simple remake of Pong.

Controls:
	-Left Player: W=Up, S=Down
	-Right Player: Up Arrow=Up, Down Arrow=Down

Copyright Jesse Anderson, 2014.